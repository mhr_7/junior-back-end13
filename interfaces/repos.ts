import { IUser } from "./descriptors";

export type Paginated<T> = {
    data: T[];
    total: number;
    page: number;
    limit: number;
};

export interface IUserRepo {
    list(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): IUser[];
    count(query: Object): number;
    create(object: IUser): IUser;
    read(username: string): IUser;
    update(username: string, object: Partial<IUser>): IUser;
    delete(username: string): boolean;
    clear(): void;
}
