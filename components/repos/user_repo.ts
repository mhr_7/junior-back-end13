import { IUserRepo } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";
import * as _ from "lodash";
import { Component } from "merapi";

export default class UserRepo extends Component implements IUserRepo {
    constructor() {
        super();
    }

    private users: { [username: string]: IUser } = {};

    public list(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): IUser[] {}

    public count(query: Object): number {}

    public create(object: IUser): IUser {
        const { username } = object;

        if (this.users[username]) {
            return null;
        }

        this.users[username] = object;
        return object;
    }

    public read(username: string): IUser {}

    public update(username: string, object: Partial<IUser>): IUser {}

    public delete(username: string): boolean {}

    public clear(): void {
        this.users = {};
    }
}
